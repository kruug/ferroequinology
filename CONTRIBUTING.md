# Contributing

When contributing to this repository, please first discuss the change you wish to make via issue before making a change.

To start working on any changes, please create a new feature branch:

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

If you need additional help, here are a couple of friendly tutorials: http://makeapullrequest.com/ and http://www.firsttimersonly.com/.

## Pull Request Process

1. Ensure any install or build dependencies are removed before the end of the layer when doing a build.
2. Update the README.md with details of changes to the interface, this includes new environment variables, exposed ports, useful file locations and container parameters.
3. Increase the version numbers in any examples files and the README.md to the new version that this Pull Request would represent. The versioning scheme we use is [SemVer](http://semver.org/).

## How to Report a Bug

Any security issues should be submitted directly to security@kruug.org.
In order to determine whether you are dealing with a security issue, ask yourself these two questions:
 * Can I access something that's not mine, or something I shouldn't have access to?
 * Can I disable something for other people?

If the answer to either of those two questions are "yes", then you're probably dealing with a security issue. Note that even if you answer "no" to both questions, you may still be dealing with a security issue, so if you're unsure, just email us at security@kruug.org.

###When filing an issue, make sure to answer these five questions:
 1. What version of .Net are you using?
 2. What operating system and processor architecture are you using?
 3. What did you do?
 4. What did you expect to see?
 5. What did you see instead?

If you find yourself wishing for a feature that doesn't exist in FerroEquinology, you are probably not alone. There are bound to be others out there with similar needs. Open an issue on our issues list on GitLab which describes the feature you would like to see, why you need it, and how it should work.

## Code of Conduct

#### Guidelines

* Please be respectful to one another.
* Many contributors are volunteering their time.  We might not get to your issue right away.  Be patient.
* Other contributors have different backgrounds and perspectives than you do. Diversity is a community strength.
* We will disagree sometimes.  That's ok.  When this happens, assume that the person with whom you disagree is a smart person with good reasons for believing something different.
* Everyone has bad days.  If you find yourself about to be mean to someone, take a break and cool off.  This project will still be here later.
* When you make a mistake, apologize.


#### We will not tolerate

* Sexualized language or imagery
* Hate speech
* Personal attacks
* Trolling or insulting/derogatory comments
* Public or private harassment
* Publishing other's private information
* Plagiarism
* Other unethical or unprofessional conduct

#### Enforcement

Project maintainers commit themselves to fairly enforcing this Code of Conduct. This means we may:

* Edit or delete harmful comments, code, issues, or other contributions
* Ban harmful users from the project, temporarily or permanently

This Code of Conduct applies both within project spaces and in public spaces when an individual is representing the project or its community.

Instances of unacceptable behavior may be reported privately to the project maintainers.
