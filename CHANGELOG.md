# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2019-11-22]
### Added
 - README file.
 - CHANGELOG file.
 - Contributing guide, which includes a Code of Conduct.
 - Started Visual Studio project.

### Fixed
 - Nothing to fix, initial commit

### Changed
 - Nothing changed, initial commit

### Removed
 - Nothing removed, initial commit