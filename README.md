# FerroEquinology

FerroEquinology aims to be an open source re-implementation of Empire Builder Pronto.  This will be a digital version of the Empire Builder/Rails series board game originally released by Mayfair Games.

## Getting Started

To get started, follow the [First Contributions](https://github.com/firstcontributions/first-contributions) guide.  This will walk you through forking, cloning, branching, commiting, and submitting your changes for review.

### Prerequisites

No prerequisites instructions yet.

### Installing

No installation instructions yet.

## Deployment

No deployment instructions yet.

## Built With

* [Visual Studio 2019](https://visualstudio.microsoft.com/vs/) - IDE Used
* .Net 4.7.2

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/kruug/ferroequinology/tags). 

## Authors

* **Viktor Kruug** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
